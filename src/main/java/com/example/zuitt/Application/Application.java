package com.example.zuitt.Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "Jake") String name){
		return String.format("Hello %s!", name);
	}
	//http://localhost:8080/hello?name=mike


	@GetMapping("/myshape")
	public String myshape(@RequestParam(value = "shape", defaultValue = "error") String shape, int numberOfSides){
		return String.format("Your shape is " +  shape + ". It has " + numberOfSides + " sides.");
	}
	//http://localhost:8080/myshape?shape=triangle&numberOfSides=3

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "error") String user){
		return  String.format("Hi "+ user +"!");
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name", defaultValue = "error") String name, int age){
		return  String.format( "I am "+ name +" is " + age +" years old.");
	}


}

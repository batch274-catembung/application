package com.example.zuitt.Application.models;

import javax.persistence.*;

//mark this java object as the representation of ta db  table via @Entity
@Entity
@Table(name="posts")
//creation or designation of table name via @Table
public class Post {
//    inidicate that this property represent the primary via @Id
    @id
//    values fot this property will be auto-incremented
    @GeneratedValue
    private Long id;


    @column
    private String title;

    @column
    private String content;

//    default constructor, this is needed when retrieving posts
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }







}


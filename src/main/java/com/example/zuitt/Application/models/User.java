package com.example.zuitt.Application.models;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {
    @id

    @GeneratedValue
    private Long id;

    @column
    private String username;

    @column
    private String password;

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }











}

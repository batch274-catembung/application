package com.example.zuitt.Application.repositories;

/*An interface contains behavior that class implements
 * An interface marked @Repository contains methods for database manipulation
 * by extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving
 * updating, and deleting
 * */

import com.example.zuitt.Application.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CrudRepository<User, Object> {

}
